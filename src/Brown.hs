{-# LANGUAGE TemplateHaskell, BangPatterns #-}

import Codec.Picture
import Codec.Picture.Types
import Codec.Picture.Metadata as M hiding (foldMap)
import Control.DeepSeq
import Control.Monad
import Control.Monad.Primitive
import Control.Monad.Reader
import Control.Parallel.Strategies
import Data.ByteString.Lazy.Char8 (ByteString)
import Data.Array as A
import qualified Data.ByteString.Lazy.Char8 as BS
import Data.List as L
import Data.Maybe
import Data.Tuple
import qualified Data.Vector.Storable as V
import Data.Word
import Debug.Trace
import Graphics.Gnuplot.Simple
import Lens.Micro 
import Lens.Micro.Mtl
import Lens.Micro.TH
import Numeric
import System.CPUTime
import System.Environment
import System.Random.Mersenne
import System.Time

-- type R = State StdGen
-- 
-- runR = flip runState 
-- 
-- rUnit :: R Double
-- rUnit = state (randomR (-1,1))
-- 
-- brownStep :: Int -> Double -> Double -> R Double
-- brownStep n x0 x1 = do r <- rUnit
--                        return $ (x0 + x1) / 2 + r / (2 ^ n) 

data Pars = Pars { 
  _seed :: String,
  _stdGen :: MTGen,
  _hurst :: Double
  } 
makeLenses ''Pars

type R = ReaderT Pars IO

runRS :: R a -> Double -> Word32 -> IO (a, String)
runRS op h s = do 
    g <- newMTGen (Just s)
    x <- runReaderT op (Pars (show s) g h)
    return (x, L.head (L.words $ show s))

runR :: R a -> Double -> IO (a, String)
runR op h = do 
    ct <- getCPUTime
    (TOD sec psec) <- getClockTime
    let s = fromIntegral $ sec * 1013904242 + psec + ct
    runRS op h s

uniform :: R Double
uniform = liftM (\x -> 2 * x - 1) $ ask >>= (liftIO . random . _stdGen) 

avg :: Fractional a => [a] -> a
avg xs = L.sum xs / fromIntegral (L.length xs)

normal :: R Double
normal = liftM avg (replicateM 6 uniform)

-- brownF :: Int -> Double -> Double -> Double
-- brownF n r h 

brownDiff2 :: Int -> R Double
brownDiff2 n = do r <- normal
                  h <- view hurst
                  return $ r * sqrt (1 - 2**(2*h - 2) / 2**h) * ((1/2 ** (h/2))^n)

brownStep :: Int -> Double -> Double -> R Double 
brownStep n x0 x1 = do r <- normal
                       h <- view hurst
                       let d = sqrt (1 - 2 ** (2 * h - 2) / 2 ** h) * ((1 / 2 ** h) ^ n)
                       return $ (x0 + x1) / 2 + r * d

brown1 :: Int -> R [Double]
brown1 n | n < 0 = return []
         | otherwise = do x <- normal
                          x' <- normal
                          b (n-1) x x'
  where
  b n' x0 x1
    | n' < 0 = return [x0,x1]
    | otherwise = do
        xmid <- brownStep (n-n'-1) x0 x1
        lh <- b (n'-1) x0 xmid
        rh <- b (n'-1) xmid x1
        return (lh ++ L.drop 1 rh)

brown2 :: Int -> R (Array (Int, Int) Double)
brown2 n = do
    arr <- A.listArray ends `liftM` replicateM (n' ^ 2) normal
    let ul = ((0,0), arr ! (0,0))
        ur = ((rlim,0), arr ! (rlim,0))
        ll = ((0,rlim), arr ! (0,rlim))
        lr = ((rlim,rlim), arr ! (rlim, rlim))
    return $ ul `seq` (array ends $ [ul,ur,ll,lr] ++ b arr ul ur ll lr)
    where
    n' = 2 ^ n + 1
    rlim = n' - 1
    ends = ((0,0), (rlim, rlim))
    bfold _ ((i,j), left) ((i',j'), up) 
      | i == i' = ((i, (j + j') `quot` 2), avg [left,up])
      | j == j' = (((i + i') `quot` 2, j), avg [left,up])
      | otherwise = ((i',j), avg [left,up])
    b arr ul ur ll lr 
      | ul ^._1._1 < ur ^._1._1,  -- ensure correct distances
        ll ^._1._1 < lr ^._1._1, 
        ul ^._1._2 < ll ^._1._2, 
        ur ^._1._2 < lr ^._1._2 = 
            let u = bfold arr ul ur
                l = bfold arr ul ll
                c = bfold arr l u 
                d = bfold arr ll c
                r = bfold arr c ur
            in [u,l,c,d,r]        -- new points
                ++ b arr ul u l c -- upper left subsection
                ++ b arr u ur c r -- upper right
                ++ b arr l c ll d -- lower left
                ++ b arr c r d lr -- lower right
      | otherwise = []

brown2'' :: Int -> R (Array (Int, Int) Double)
brown2'' n = do
    arr <- A.listArray ends `liftM` replicateM (n' ^ 2) normal
    return $ array ends $ 
        b arr (fmap ((,) <*> (arr !)) $ join (liftM2 (,)) [0, rlim]) 1
  where
  n' = 2 ^ n + 1
  rlim = n' - 1
  ends = ((0,0), (rlim, rlim))
  b arr xs n''
    | n'' > n = traceShow xs $ xs
    | otherwise = 
      let diags = iterDiag xs `p` diagNth n n''
      in diags `seq` b arr (iterLong diags `p` longNth n n'') (n'' + 1) 
    where
    iterDiag ys (i,j) = (,) (i,j) $ avg [ fromJust (L.lookup (i',j') ys) 
          | i' <- [i-1,i+1], 
            j' <- [j-1,j+1], 
            i' >= 0 && j' >= 0 && i' <= rlim && j' <= rlim]
    iterLong ys (i,j) = (,) (i,j) $ avg [ fromJust (L.lookup (i',j') ys)
          | (i', j') <- [(i,j-1), (i,j+1), (i-1,j), (i+1,j)],
            i' >= 0 && j' >= 0 && i' <= rlim && j' <= rlim] 
  f `p` xs = fmap f xs `using` parList rseq

diagNth :: Int -> Int -> [(Int, Int)]
diagNth n n'' = prod [firs, firs + step .. rlimit]
  where
  prod = join (liftM2 (,))
  n' = 2 ^ n + 1
  rlimit = n' - 1
  firs = 2 ^ (n - n'')
  step = 2 ^ (n - n'' + 1)

longNth :: Int -> Int -> [(Int, Int)]
longNth n n'' = dubprod [firs, firs + step .. rlimit] [0, step .. rlimit]
  where
  dubprod xs ys = prod xs ys ++ prod ys xs 
  prod = liftM2 (,)
  n' = 2 ^ n + 1
  rlimit = n' - 1
  firs = 2 ^ (n - n'')
  step = 2 ^ (n - n'' + 1)

rand :: Int -> R (Array (Int, Int) Double)
rand n = listArray ((0,0),(2^n+1, 2^n+1)) `liftM` replicateM (8^(2*n)) normal

brown2' :: Int -> R (Array (Int, Int) Double)
brown2' n | n < 0 = return $ listArray ((0,0),(0,0)) [0]
         | otherwise = do
             ul <- normal
             ur <- normal
             ll <- normal 
             lr <- normal
             b (n - 1) $ listArray ((0,0),(1,1)) [ul,ll,ur,lr]
  where
  b :: Int -> Array (Int, Int) Double -> R (Array (Int, Int) Double)
  b n' arr
    | n' < 0 = return arr
    | otherwise = do
        let origs = fmap (\((i,j),x) -> ((2*i,2*j), x)) (A.assocs arr)
            (_,(w,h)) = bounds arr
            (neww, newh) = (2*w, 2*h)
        cs <- forM [(i,j) | i <- range (0,w-1), j <- range (0,h-1)] $ \(i,j) -> do
            let zavg = avg [arr ! (i',j') | i' <- [i,i+1], j' <- [j,j+1], i' > 0 && j' > 0 && i' <= w && j' <= h]
            z <- brownDiff2 (n-n'-1)
            return $ ((2*i+1, 2*j+1), zavg + z)
        os <- forM [(i,j) | i <- range (0,neww), j <- range (0,newh), (i + j) `mod` 2 == 1] $ \(i,j) -> do
            let zs = [(i',j') | (i',j') <- [(i-1,j),(i+1,j),(i,j-1),(i,j+1)], i' >= 0 && j' >= 0 && i' <= neww && j' <= newh]
                zavg = avg $ fmap (\t -> fromJust $ L.lookup t (origs ++ cs)) zs
            z <- brownDiff2 (n-n')
            return $ ((i,j), zavg + z)
        let xs = origs ++ cs ++ os
        xs `deepseq` b (n'-2) (array ((0,0),(neww,newh)) xs)

create1D :: Double -> IO () 
create1D h = do
    (b, g) <- runR (brown1 8) h
    img <- thawImage $ generateImage (\_ _ -> PixelRGBA8 0 0 0 255) (L.length b) 16
    zipWithM (\i v -> forM [0..15] $ \y -> do
        writePixel img i y (gray v)) [0..] b 
    png <- encodePngWithMetadata 
        (M.insert Comment g empty) `liftM` freezeImage img
    BS.writeFile "asd.png" png

create2D :: Int -> Double -> Maybe Word32 -> IO ()
create2D n h ms = do
    (arr, g) <- case ms of 
        Nothing -> runR (brown2' n) h -- return $ (listArray ((0,0), (2^10,2^10)) (L.repeat 127), "...") -- runR (rand 14) h
        Just s -> runRS (brown2' n) h s
    let (_,(wid,hei)) = bounds arr
        img = generateImage (\i j -> grad xRes' (arr ! (i,j))) (wid+1) (hei+1)
        png = encodePngWithMetadata (M.insert Comment g empty) img
    BS.writeFile (g ++ "-" ++ show h ++ ".png") png

main :: IO ()
main = do
    n : h : opts <- getArgs
    case opts of
        s:_ -> create2D (read n) (read h) (Just $ read s)
        _ -> create2D (read n) (read h) Nothing

gnu :: Int -> Double -> IO ()
gnu n h = do
    (b, _) <- runR (brown1 n) h
    plotList [Aspect (Ratio 1.0), BoxAspect (Ratio 1.0), Size (Scale 1.0)] b

grayGrad :: Grad
grayGrad = [(0, PixelRGBA8 0 0 0 255), (1, PixelRGBA8 255 255 255 255)]

gray :: Double -> PixelRGBA8
gray f = let d = round (127.5 * f + 127.5) in PixelRGBA8 d d d 255

type Grad = [(Double, PixelRGBA8)]

grad :: Grad -> Double -> PixelRGBA8
grad unsortedXs t = case L.partition ((==t') . fst) <$> L.partition ((<t') . fst) xs of 
    (_, ((_,p):_, _)) -> p
    ([], (_, [])) -> gray t
    (lt, (_, [])) -> snd $ L.last lt
    ([], (_, ge)) -> snd $ L.head ge
    (lt, (_, ge)) -> let (d, p) = L.last lt
                         (d',p') = L.head ge
                     in if d' == t' then p'
                     else lin (abs $ t' - d) p (abs $ t' - d') p' 
    where
    t' = (t + 1) / 2
    xs = L.sortOn fst unsortedXs
    lin d (PixelRGBA8 r g b a) d' (PixelRGBA8 r' g' b' a') = PixelRGBA8 (lin' r r') (lin' g g') 
                                                                        (lin' b b') (max a a')
        where
        lin' x y = round $ (fromIntegral x / d + fromIntegral y / d') / (1/d + 1/d')

type Color = PixelRGBA8

xRes :: Grad
xRes = [(0, PixelRGBA8 0x27 0x2d 0x30 0xff), (1, PixelRGBA8 0x0 0xb0 0xef 0xff)]

xRes' :: Grad
xRes' = [(0.0, PixelRGBA8 0x11 0x14 0x15 0xff), 
         (0.4, PixelRGBA8 0x27 0x2d 0x30 0xff), 
         (0.8, PixelRGBA8 0x0  0xb0 0xef 0xff),
         (0.9, PixelRGBA8 0x0  0xb0 0xef 0xff),
         -- (0.92, PixelRGBA8 0x7d 0x7d 0x7d 0xff),
         (1.0, PixelRGBA8 0x7d 0x7d 0x7d 0xff)]

xRed :: Grad
xRed = [(0, PixelRGBA8 0x27 0x2d 0x30 0xff), (1, PixelRGBA8 0xcd 0x5c 0x63 0xff)]
